#!/usr/bin/python3
import json
from pathlib import Path
from sys import argv
from zipfile import ZipFile


def dict_diff(source, target):
    if not isinstance(target, dict) or not isinstance(source, dict):
        return target

    result = {key: None for key in source if key not in target}

    for key, value in target.items():
        if key not in source:
            result[key] = value
        elif value != source[key]:
            result[key] = dict_diff(source[key], value)

    return result


# only to fake the data
def file_entry(name, hashType=None, hsh=None, size=None):
    meta = {}
    meta["name"] = name
    if hsh:
        meta[hashType] = hsh
    else:
        # common.sha256sum(name)
        meta[
            "sha256"
        ] = "b1f27fa87f8cabca50cdcd462a0f500d79d883b965a498d0e49eea560b39be1f"
    if size:
        meta["size"] = size
    else:
        meta["size"] = 124
    return meta


def convert_localized(localized, appid):
    metadata = {}
    whatsNew = {}

    for locale, values in localized.items():
        for e in ("name", "summary", "description", "video"):
            if e in values:
                if e not in metadata:
                    metadata[e] = {}
                metadata[e][locale] = values[e]

        for e in (
            "featureGraphic",
            "icon",
            "promoGraphic",
            "tvBanner",
        ):
            if e in values:
                if e not in metadata:
                    metadata[e] = {}
                metadata[e][locale] = file_entry(
                    "/{}/{}/{}".format(appid, locale, values[e])
                )

        for e in (
            "phoneScreenshots",
            "sevenInchScreenshots",
            "tenInchScreenshots",
            "tvScreenshots",
            "wearScreenshots",
        ):
            if e in values:
                en = e.replace("Screenshots", "")

                if "screenshots" not in metadata:
                    metadata["screenshots"] = {}

                if en not in metadata["screenshots"]:
                    metadata["screenshots"][en] = {}
                metadata["screenshots"][en][locale] = [
                    file_entry("/{}/{}/{}/{}".format(appid, locale, e, screenshot))
                    for screenshot in values[e]
                ]

        if "whatsNew" in values:
            whatsNew[locale] = values["whatsNew"]

    # *Screenshots -> screenshots
    # whatsNew -> per version
    check_missing(
        {k for x in localized.values() for k in x},
        [metadata],
        "localized",
        [
            "phoneScreenshots",
            "sevenInchScreenshots",
            "tenInchScreenshots",
            "tvScreenshots",
            "wearScreenshots",
            "whatsNew",
        ],
    )

    return metadata, whatsNew


def package_metadata(app):
    metadata = {}
    # {y for a in [x.keys() for x in oldapps"]] for y in a}
    for e in (
        "added",
        "categories",
        "changelog",
        "issueTracker",
        "lastUpdated",
        "license",
        "sourceCode",
        "translation",
        "webSite",
        "authorEmail",
        "authorName",
        "authorPhone",
        "authorWebSite",
        "bitcoin",
        "flattrID",
        "liberapay",
        "liberapayID",
        "litecoin",
        "openCollective",
    ):
        if e in app:
            metadata[e] = app[e]

    # fdroidserver/metadata.py App default
    if metadata["license"] == "Unknown":
        del metadata["license"]

    if "donate" in app:
        metadata["donate"] = [app["donate"]]

    whatsNew = {}
    if "localized" in app:
        loc, whatsNew = convert_localized(app["localized"], app["packageName"])
        metadata.update(loc)

    # TODO always overwrite localized?
    for e in ("name", "summary", "description"):
        if e in app:
            metadata[e] = {"en-US": app[e]}

    # TODO handle different resolutions
    if "icon" in app:
        metadata["icon"] = {"en-US": file_entry("/icons/{}".format(app["icon"]))}

    # antiFeatures -> per version
    # binaries -> drop?
    # suggestedVersionName, suggestedVersionCode -> channel
    # packageName -> drop?
    # license -> optional
    # localized -> split up
    check_missing(
        app,
        [metadata],
        "metadata",
        [
            "antiFeatures",
            "binaries",
            "suggestedVersionName",
            "suggestedVersionCode",
            "packageName",
            "license",
            "localized",
        ],
    )

    return metadata, whatsNew


def convert_version(version, app):
    ver = {}
    if "added" in version:
        ver["added"] = version["added"]
    else:
        ver["added"] = 0

    ver["file"] = file_entry(
        "/{}".format(version["apkName"]),
        version["hashType"],
        version["hash"],
        version["size"],
    )

    if "srcname" in version:
        ver["src"] = file_entry("/{}".format(version["srcname"]))

    if "obbMainFile" in version:
        ver["obbMainFile"] = file_entry(
            "/{}".format(version["obbMainFile"]),
            "sha256",
            version["obbMainFileSha256"],
            124,
        )

    if "obbPatchFile" in version:
        ver["obbPatchFile"] = file_entry(
            "/{}".format(version["obbPatchFile"]),
            "sha256",
            version["obbPatchFileSha256"],
            124,
        )

    ver["manifest"] = manifest = {}

    for e in (
        "nativecode",
        "versionName",
        "maxSdkVersion",
    ):
        if e in version:
            manifest[e] = version[e]

    if "versionCode" in version:
        manifest["versionCode"] = int(version["versionCode"])

    # TODO get version from manifest, default (0) is omitted
    if "features" in version:
        manifest["features"] = features = []
        for feature in version["features"]:
            if len(feature) % 2 == 0:
                features.append({"name": feature, "version": 1})
            else:
                features.append({"name": feature})

    if "minSdkVersion" in version:
        manifest["usesSdk"] = {}
        manifest["usesSdk"]["minSdkVersion"] = version["minSdkVersion"]
        if "targetSdkVersion" in version:
            manifest["usesSdk"]["targetSdkVersion"] = version["targetSdkVersion"]
        else:
            # https://developer.android.com/guide/topics/manifest/uses-sdk-element.html#target
            manifest["usesSdk"]["targetSdkVersion"] = manifest["usesSdk"]["minSdkVersion"]

    if "signer" in version:
        manifest["signer"] = {"sha256": [version["signer"]]}

    for e in ("uses-permission", "uses-permission-sdk-23"):
        en = e.replace("uses-permission", "usesPermission").replace("-sdk-23", "Sdk23")
        if e in version:
            manifest[en] = []
            for perm in version[e]:
                if perm[1]:
                    manifest[en].append({"name": perm[0], "maxSdkVersion": perm[1]})
                else:
                    manifest[en].append({"name": perm[0]})

    # TODO: get reasons from fdroiddata
    if "antiFeatures" in app:
        ver["antiFeatures"] = {}
        for af in app["antiFeatures"]:
            if af.startswith("N"):
                ver["antiFeatures"][af] = {"en-US": "reason"}
            else:
                ver["antiFeatures"][af] = {}

    if "antiFeatures" in version:
        if "antiFeatures" not in ver:
            ver["antiFeatures"] = {}
        for af in version["antiFeatures"]:
            ver["antiFeatures"][af] = {"en-US": "reason"}

    if "versionCode" in version:
        if int(version["versionCode"]) > int(app["suggestedVersionCode"]):
            ver["releaseChannels"] = ["Beta"]

    if "whatsNew" in version:
        ver["whatsNew"] = version["whatsNew"]

    # apkName, hash, hashType, size -> file
    # minSdkVersion, targetSdkVersion -> usesSdk
    # obb*Sha256 -> obb*
    # packageName, sig -> removed
    # srcname -> src
    # uses-permission -> usesPermission
    # uses-permission-sdk-23 -> usesPermissionSdk23
    check_missing(
        version,
        [ver, manifest],
        "version",
        [
            "apkName",
            "hash",
            "hashType",
            "minSdkVersion",
            "obbMainFileSha256",
            "obbPatchFileSha256",
            "packageName",
            "sig",
            "size",
            "srcname",
            "targetSdkVersion",
            "uses-permission",
            "uses-permission-sdk-23",
        ],
    )

    return ver


def v2_repo(repodict):
    repo = {}

    repo["name"] = {"en-US": repodict["name"]}
    repo["icon"] = {"en-US": file_entry("/icons/{}".format(repodict["icon"]))}
    repo["address"] = repodict["address"]
    repo["webBaseUrl"] = "https://f-droid.org/packages/"
    repo["description"] = {"en-US": repodict["description"]}
    if "mirrors" in repodict:
        repo["mirrors"] = [{"url": mirror} for mirror in repodict["mirrors"]]

    # only for the conversion
    repo["timestamp"] = repodict["timestamp"]
    repo["version"] = repodict["version"]
    if "maxage" in repodict:
        repo["maxAge"] = repodict["maxage"]

    repo["antiFeatures"] = af = {}
    af["Advertising"] = {}
    af["Advertising"]["icon"] = file_entry("/icons/advertising-icon.png")
    af["Advertising"]["name"] = {"en-US": "Advertising"}
    af["Advertising"]["description"] = {"en-US": "Apps that contains advertising."}

    repo["categories"] = cat = {}
    cat["System"] = {}
    cat["System"]["icon"] = file_entry("/icons/category_games.png")
    cat["System"]["name"] = {"en-US": "System"}
    cat["System"]["description"] = {"en-US": "Apps for your System"}

    repo["releaseChannels"] = channel = {}
    channel["Beta"] = {}
    channel["Beta"]["name"] = {"en-US": "Beta"}
    channel["Beta"]["description"] = {"en-US": "Beta versions"}

    return repo


def convert(old):
    index = {}

    index["repo"] = v2_repo(old["repo"])

    # make requests optional
    if "requests" in old and old["requests"]["install"] or old["requests"]["uninstall"]:
        repo["requests"] = old["requests"]

    # for testing
    # repo["requests"]["install"] = ["org.fdroid.fdroid", "ws.xsoh.etar"]

    index["packages"] = {}
    apps = {app["packageName"]: app for app in old["apps"]}

    #old["packages"] = {k: old["packages"][k] for k in list(old["packages"])[:3]}
    for name, versions in old["packages"].items():
        index["packages"][name] = package = {}
        package["metadata"], whatsNew = package_metadata(apps[name])

        if "signer" in versions[0]:
            package["metadata"]["preferredSigner"] = versions[0]["signer"]

        if whatsNew:
            versions[0]["whatsNew"] = whatsNew

        package["versions"] = {
            version["hash"]: convert_version(version, apps[name])
            for version in versions
        }

    # requests are optional, apps is merged into packages
    check_missing(old, [index], "root", ["requests", "apps"])
    # maxage -> maxAge
    check_missing(old["repo"], [index["repo"]], "repo", ["maxage"])

    return index


# Should be read from fastlane, here only to make it work with index-v1
def copy_whatsnew(indexes):
    for old, new in zip(indexes, indexes[1:]):
        for app in old["packages"]:
            if app in new["packages"]:
                for version in old["packages"][app]["versions"]:
                    if (
                        "whatsNew" in old["packages"][app]["versions"][version]
                        and version in new["packages"][app]["versions"]
                    ):
                        new["packages"][app]["versions"][version]["whatsNew"] = old[
                            "packages"
                        ][app]["versions"][version]["whatsNew"]


def check_missing(old, new, key, ignore):
    new_keys = [x for j in new for x in j]
    old_keys = [k for k in old if k not in new_keys]
    # optional
    for ign in ignore:
        if ign in old_keys:
            old_keys.remove(ign)
    if old_keys:
        print("missing keys", key, old_keys)
        # print(old)
        # print(new)


def load_index(filename):
    if filename.endswith(".jar"):
        with ZipFile(filename) as zf:
            return json.loads(zf.read("index-v1.json"))
    else:
        with open(filename, encoding="utf-8") as fp:
            return json.load(fp)


def main():
    indexes = [convert(load_index(fn)) for fn in argv[1:]]
    copy_whatsnew(indexes)

    new = indexes.pop()

    entry = {}
    entry["timestamp"] = new["repo"]["timestamp"]

    # use fdroidserver config values in future
    entry["version"] = new["repo"]["version"]
    del new["repo"]["version"]
    # TODO make maxAge non optional?
    if "maxAge" in new["repo"]:
        entry["maxAge"] = new["repo"]["maxAge"]
        del new["repo"]["maxAge"]
    for old in indexes:
        del old["repo"]["version"]
        if "maxAge" in old["repo"]:
            del old["repo"]["maxAge"]

    Path("index-v2.json").write_text(
        json.dumps(new, indent=2, ensure_ascii=False), encoding="utf-8"
    )
    entry["index"] = file_entry("/index-v2.json")
    entry["index"]["numPackages"] = len(new.get("packages", []))

    entry["diffs"] = {}
    for old in indexes:
        diff_name = str(old["repo"]["timestamp"]) + ".json"
        diff = dict_diff(old, new)
        Path(diff_name).write_text(
            json.dumps(diff, indent=2, ensure_ascii=False), encoding="utf-8"
        )

        entry["diffs"][old["repo"]["timestamp"]] = file_entry("/{}".format(diff_name))
        entry["diffs"][old["repo"]["timestamp"]]["numPackages"] = len(diff.get("packages", []))

    Path("entry.json").write_text(
        json.dumps(entry, indent=2, ensure_ascii=False), encoding="utf-8"
    )


if __name__ == "__main__":
    main()
